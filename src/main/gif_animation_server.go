package main

import (
	"image"
	"image/color"
	"image/gif"
	"io"
	"log"
	"math"
	"math/rand"
	"net/http"
	"strconv"
)

var palette = []color.Color{color.White, color.Black}

const (
	greenIndex = 1
)

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	cycles := 5
	res := 0.001
	size := 100
	nframes := 64
	delay := 8

	if err := r.ParseForm; err != nil {
		log.Print(err)
	}

	for param, values := range r.Form {
		value := values[0]

		// We are going to convert existing parameters only if the conversion
		// succeeds. Otherwise just ignore them.
		switch param {
		case "cycles":
			if i, err := strconv.Atoi(value); err == nil {
				cycles = i
			}
		case "res":
			if f, err := strconv.ParseFloat(value, 64); err == nil {
				res = f
			}
		case "size":
			if i, err := strconv.Atoi(value); err == nil {
				size = i
			}
		case "nframes":
			if i, err := strconv.Atoi(value); err == nil {
				nframes = i
			}
		case "delay":
			if i, err := strconv.Atoi(value); err == nil {
				delay = i
			}
		}
	}

	lissajous(w, cycles, res, size, nframes, delay)
}

func lissajous(out io.Writer, cycles int, res float64, size int, nframes int, delay int) {
	freq := rand.Float64()*3.0
	anim := gif.GIF{LoopCount:nframes}
	phase := 0.0

	for i := 0; i < nframes; i++{
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)

		for t := 0.0; t < float64(cycles*2)*math.Pi; t += res{
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)
			img.SetColorIndex(size + int(x*float64(size)+0.5), size + int(y*float64(size)+0.5), greenIndex)
		}

		phase += 0.1
		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)
	}

	gif.EncodeAll(out, &anim) // NOTE: ignoring encoding errors
}

