package main

import(
	"fmt"
	"log"
	"net/http"
)

func main(){
	http.HandleFunc("/", handler) //handle request calss
	log.Fatal(http.ListenAndServe("localhost:8000", nil))

}

// handle the path component of the request urls
func handler(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "URL.Path = %q\n", r.URL.Path)

}